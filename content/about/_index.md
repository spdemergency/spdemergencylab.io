+++
date = "2016-11-05T21:05:33+05:30"
title = "About me"
+++


![This is me][1]

#### Some Anecdotes

March happens to be Autoimmune Disease Awareness Month, and here's a thing I did about a thing I went through.
Two years ago, I ended up with a skin disorder known as pityriasis rosea, which leveled up into lichen planus with a bit of psoriasis on the side. A proper three-for-one autoimmune combo.
What followed was six months of angry red-purple-brown spots and lesions all over my body (except the face), extreme itchiness, four dermatologists, a million different creams and tablets, rethinking of life choices, and learning to dry-swallow antihistamines on the go. I'm alright now, just left with scars that are slowly fading.
These disorders are more common than we think, with little information and awareness. They are NOT contagious, and can be maintained, if not cured. Any sort of externally visible skin disorder can cause a lot of social anxiety and insecurity, and I've only just learned to live with it. But I request people to have a more open mind about scars and other skin blemishes, and to be more tactful while addressing them, if you do need to. Most of us are happy to talk about it and provide more information.
Here are a few of the many, many thoughts that kept me up (along with the itching of course).
I don't even do any sort of sport, so I don't know why the Olympics thing bummed me out back then.

[1]: /img/about.jpg
